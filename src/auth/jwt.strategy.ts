import { PassportStrategy } from '@nestjs/passport'
import { Injectable } from '@nestjs/common'
import { ExtractJwt, Strategy } from 'passport-jwt'

export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(), // lays token trong request
            ignoreExceptions: false, // bo qua het han token 
            secretOrKey: 'SECRET' // protect this , move tho env var
        })
    }

    async validate(payload: any) {

        // const user = await this.usersService.getById(payload.sub)
        return { id: payload.sub, name: payload.name }
    }
}